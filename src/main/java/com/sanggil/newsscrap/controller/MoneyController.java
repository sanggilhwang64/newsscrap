package com.sanggil.newsscrap.controller;

import com.sanggil.newsscrap.model.MoneyChangeRequest;
import com.sanggil.newsscrap.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/money")
@RequiredArgsConstructor
public class MoneyController {
    private final MoneyService moneyService;

    @PostMapping("/change")
    public String peopleChange(@RequestBody MoneyChangeRequest request) {
        String result = moneyService.convertMoney(request.getMoney());
        return result;
    }

    @GetMapping("/pay-back")
    public String peoplePayBack() {
        return "환불되었습니다. 고객님";
    }
}