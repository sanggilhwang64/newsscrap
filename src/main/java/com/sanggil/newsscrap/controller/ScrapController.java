package com.sanggil.newsscrap.controller;

import com.sanggil.newsscrap.model.NewsItem;
import com.sanggil.newsscrap.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<NewsItem> getHtml() throws IOException {
        List<NewsItem> result = scrapService.run();
        return result;
    }
}
