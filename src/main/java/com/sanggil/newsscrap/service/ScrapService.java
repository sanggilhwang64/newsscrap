package com.sanggil.newsscrap.service;

import com.sanggil.newsscrap.model.NewsItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document getFullHtml() throws IOException {
        String url = "http://www.astronomer.rocks/"; // 접속할 주소
        Connection connection = Jsoup.connect(url); // 추가적인 기능 url에 접속

        Document document = connection.get(); // 위에 박스한테 문서 가져오기 문서 없으면 비상구 탈출 alt+enter

        return document; // 문서 돌려주기
    }

    private List<Element> parseHtml(Document document) {
        Elements elements = document.getElementsByClass("auto-article");
        //
        List<Element> tempResult = new LinkedList<>();

        for (Element item : elements) {
            Elements lis = item.getElementsByTag("li");
            for (Element item2 : lis) {
                tempResult.add(item2);
            }
        }

        return tempResult;
    }

    private List<NewsItem> makeResult (List<Element> list) {
        List<NewsItem> result = new LinkedList<>(); //

        for (Element item : list) {
            Elements checkContents = item.getElementsByClass("flow-hidden"); // flow-hidden 가져오기
            if (checkContents.size() == 0) { // flow-hidden 없으면
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB"); // auto-fontB 가져오기 정상적인 것은 auto-fontB 가지고있음
                if (checkHiddenBanner.size() >0) { // 하나라도 있으면 정상적인 것
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    // 타이틀 가져오기
                    String content = item.getElementsByClass("auto-fontB").get(0).text(); // 콘텐츠 가져오기

                    NewsItem addItem = new NewsItem(); // 새로운 빈그릇
                    addItem.setTitle(title); // 타이틀공간에 세팅 위에 있는 타이틀
                    addItem.setContent(content); // 컨텐츠공간에 세팅 위에 있는 컨텐츠

                    result.add(addItem); // "li"가 소진 될때까지 위 과정이 반복
                }
            }
        }

        return result;
    }



    public List<NewsItem> run() throws IOException {
        Document document = getFullHtml();
        List<Element> elements = parseHtml(document);
        List<NewsItem> result =makeResult(elements);

        return result;
    }
}
